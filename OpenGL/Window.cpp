#include "Window.h"


namespace Tea {

	void 	framebuffer_size_callback(GLFWwindow* window, int width, int height);
	Window::Window()
	{
	}
	Window::~Window()
	{
	}
	bool Window::CreateWindow( std::string title, int width, int height)
	{
		m_title = title;
		m_height = height;
		m_width = width;
		m_window = glfwCreateWindow(m_width, m_height, m_title.c_str(), NULL, NULL);
		if (m_window == NULL) {
			std::cout << "create window faile." << std::endl;
			glfwTerminate();
			return false;
		}
		std::cout << "创建一个窗口." << std::endl;
		OpenWindow();
		glfwSetFramebufferSizeCallback(m_window, framebuffer_size_callback);
		return true;
	}
	bool Window::OpenWindow()
	{
		glfwShowWindow(m_window);
		std::cout << "打开一个窗口." << std::endl;
		return true;
	}
	bool Window::CloseWindow()
	{
		std::cout << "关闭一个窗口." << std::endl;
		glfwSetWindowShouldClose(m_window, GLFW_TRUE);
		return true;
	}
	bool Window::DestroyWindow()
	{
		std::cout << "销毁一个窗口." << std::endl;
		glfwDestroyWindow(m_window);
		return true;
	}
	GLFWwindow* Window::getWindow()
	{
		if (m_window != NULL) {
			return m_window;
		}
		else {
			std::cout << "窗口未创建." << std::endl;
			return nullptr;
		}
	}

	void 	framebuffer_size_callback(GLFWwindow* window, int width, int height) {
		glViewport(0, 0, width, height);
	}
}
