#pragma once
#include "Common.h"

namespace Tea {
	class AbstractWindow
	{
	public:
		AbstractWindow();
		virtual ~AbstractWindow();

		virtual bool CreateWindow( std::string title,int width,int height);
		virtual bool OpenWindow();
		virtual bool CloseWindow();
		virtual bool DestroyWindow();
	public:		
		std::string m_title;
		int m_width=400;
		int m_height=380;
	};
}



