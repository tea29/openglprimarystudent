#include "Common.h"
#include "Window.h"


int main(void) {


	std::cout << "hello world!" << std::endl;
	glfwInit();
	//glfw版本的指定
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	Tea::Window window;
	window.CreateWindow("hello openGL",400,380);

	//将窗口作为opengl的上下文
	glfwMakeContextCurrent(window.getWindow());

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cout<<"load openglfunction error."<<std::endl;
		return -1;
	}
	//设置窗口大小		

	while (!glfwWindowShouldClose(window.getWindow())) {
		glfwSwapBuffers(window.getWindow());
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}

