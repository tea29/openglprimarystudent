#pragma once
#include "AbstractWindow.h"

namespace Tea {
	class Window : AbstractWindow
	{
	public:
		Window();
		~Window();
		virtual bool CreateWindow(std::string title,int width,int height);
		virtual bool OpenWindow();
		virtual bool CloseWindow();
		virtual bool DestroyWindow();
		GLFWwindow* getWindow();
	public:
		GLFWwindow* m_window;
	};
}


