#include "AbstractWindow.h"

namespace Tea {
	AbstractWindow::AbstractWindow()
	{
	}

	AbstractWindow::~AbstractWindow()
	{
	}

	bool AbstractWindow::CreateWindow(std::string title, int width, int height)
	{
		m_title = title;
		m_height = height;
		m_width = width;
		return false;
	}

	bool AbstractWindow::OpenWindow()
	{
		return false;
	}

	bool AbstractWindow::CloseWindow()
	{
		return false;
	}

	bool AbstractWindow::DestroyWindow()
	{
		return false;
	}
}


